import axios from "axios";
export const BASE_URL = 'https://hyva-server.onrender.com/api';
export const apiClient = axios.create({
    baseURL: BASE_URL
});